INSERT INTO
    CUSTOMERS
VALUES
    (64525750073, '1993-01-12', 'Ismael Contreras'),
    (03261085053, '1966-07-18', 'Ingrid Alvarez'),
    (77965806061, '1950-11-03', 'Willow Petty'),
    (65538494128, '1963-08-22', 'Kieron Kerr');

INSERT INTO
    ADDRESSES
VALUES
    ('', 'Cabo Frio', 'Jose Maria', '186', 'Rua Rio de Janeiro', 'RJ', '28920-100', 64525750073),
    ('CASA', 'Campo Grande', 'Novo Horizonte', '534', 'Travessa Guatuba', 'MS', '79035-241', 03261085053);
