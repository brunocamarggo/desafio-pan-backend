package com.brunocamarggo.desafiopanbackend.entrypoint.controller;

import com.brunocamarggo.desafiopanbackend.entrypoint.model.response.AddressHttpResponse;
import com.brunocamarggo.desafiopanbackend.entrypoint.model.response.DataHttpResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CustomerControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void shouldReturnAddress200OK() {
        ResponseEntity<DataHttpResponse<AddressHttpResponse>> response =
                restTemplate.exchange("/clientes/64525750073/endereco",
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<DataHttpResponse<AddressHttpResponse>>(){});

        DataHttpResponse<AddressHttpResponse> responseBody = response.getBody();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        assertThat(responseBody).isNotNull();
        assertThat(responseBody.getData()).isNotNull();
        assertThat(responseBody.getData().getCep()).isEqualTo("28920-100");
        assertThat(responseBody.getData().getLogradouro()).isEqualTo("Rua Rio de Janeiro");
        assertThat(responseBody.getData().getBairro()).isEqualTo("Jose Maria");
        assertThat(responseBody.getData().getCidade()).isEqualTo("Cabo Frio");
        assertThat(responseBody.getData().getNumero()).isEqualTo("186");
        assertThat(responseBody.getData().getUf()).isEqualTo("RJ");
    }

    @Test
    public void shouldNotFoundAddress404NotFound() {
        ResponseEntity<DataHttpResponse<AddressHttpResponse>> response =
                restTemplate.exchange("/clientes/1234546789/endereco",
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<DataHttpResponse<AddressHttpResponse>>(){});

        DataHttpResponse<AddressHttpResponse> responseBody = response.getBody();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(responseBody).isNotNull();
        assertThat(responseBody.getMessage()).isEqualTo("Não há endereço para o cliente informado.");
    }

    @Test
    public void shouldUpdateCustomersAddress200OK() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>("{ \"cep\": \"99999999\", \"logradouro\": \"Street Updated\", \"cidade\": \"City Updated\", \"estado\": \"AC\", \"numero\": \"1234\", \"complemento\": \"\" }", headers);

        ResponseEntity<DataHttpResponse<AddressHttpResponse>> response =
                restTemplate.exchange("/clientes/77965806061/endereco",
                        HttpMethod.PUT,
                        entity,
                        new ParameterizedTypeReference<DataHttpResponse<AddressHttpResponse>>(){});

        DataHttpResponse<AddressHttpResponse> responseBody = response.getBody();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        assertThat(responseBody).isNotNull();
        assertThat(responseBody.getData()).isNotNull();
        assertThat(responseBody.getData().getCep()).isEqualTo("99999999");
        assertThat(responseBody.getData().getLogradouro()).isEqualTo("Street Updated");
        assertThat(responseBody.getData().getCidade()).isEqualTo("City Updated");
        assertThat(responseBody.getData().getNumero()).isEqualTo("1234");
        assertThat(responseBody.getData().getUf()).isEqualTo("AC");
    }

    @Test
    public void shouldNotFoundCustomerWhenTryingToUpdated404NotFound() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<>("{ \"cep\": \"99999999\", \"logradouro\": \"Street Updated\", \"cidade\": \"City Updated\", \"estado\": \"AC\", \"numero\": \"1234\", \"complemento\": \"\" }", headers);

        ResponseEntity<DataHttpResponse<AddressHttpResponse>> response =
                restTemplate.exchange("/clientes/1234567890/endereco",
                        HttpMethod.PUT,
                        entity,
                        new ParameterizedTypeReference<DataHttpResponse<AddressHttpResponse>>(){});


        DataHttpResponse<AddressHttpResponse> responseBody = response.getBody();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
        assertThat(responseBody).isNotNull();
        assertThat(responseBody.getMessage()).isEqualTo("Não há endereço para o cliente informado.");
    }
  
}