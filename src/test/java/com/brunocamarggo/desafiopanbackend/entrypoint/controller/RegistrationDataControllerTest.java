package com.brunocamarggo.desafiopanbackend.entrypoint.controller;

import com.brunocamarggo.desafiopanbackend.dataprovider.http.ServicosDadosIBGEClient;
import com.brunocamarggo.desafiopanbackend.dataprovider.http.ViaCepClient;
import com.brunocamarggo.desafiopanbackend.dataprovider.http.model.response.ServicosDadosIBGECityHttpResponse;
import com.brunocamarggo.desafiopanbackend.dataprovider.http.model.response.ViaCepAddressHttpResponse;
import com.brunocamarggo.desafiopanbackend.entrypoint.model.response.AddressHttpResponse;
import com.brunocamarggo.desafiopanbackend.entrypoint.model.response.DataHttpResponse;
import com.brunocamarggo.desafiopanbackend.entrypoint.model.response.StateHttpResponse;
import com.brunocamarggo.desafiopanbackend.usecase.domain.StateDomain;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class RegistrationDataControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @MockBean
    private ViaCepClient viaCepClient;

    @MockBean
    private ServicosDadosIBGEClient servicosDadosIBGEClient;

    @Test
    public void shouldReturnListOfStatesSortedBySaoPauloAndRioJaneiroThenAlphabeticalOrder200OK() {
        ResponseEntity<DataHttpResponse<List<StateHttpResponse>>> response =
                restTemplate.exchange("/estados",
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<DataHttpResponse<List<StateHttpResponse>>>(){});

        DataHttpResponse<List<StateHttpResponse>> responseBody = response.getBody();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        assertThat(responseBody).isNotNull();
        assertThat(responseBody.getData()).isNotNull();
        assertThat(responseBody.getData().size()).isEqualTo(27);
        assertThat(responseBody.getData().get(0).getNome()).isEqualTo(StateDomain.SAO_PAULO.getName());
        assertThat(responseBody.getData().get(0).getSigla()).isEqualTo(StateDomain.SAO_PAULO.getUf());
        assertThat(responseBody.getData().get(0).getIdIBGE()).isEqualTo(StateDomain.SAO_PAULO.getIBGEid());
        assertThat(responseBody.getData().get(1).getNome()).isEqualTo(StateDomain.RIO_DE_JANEIRO.getName());
        assertThat(responseBody.getData().get(1).getSigla()).isEqualTo(StateDomain.RIO_DE_JANEIRO.getUf());
        assertThat(responseBody.getData().get(1).getIdIBGE()).isEqualTo(StateDomain.RIO_DE_JANEIRO.getIBGEid());
        assertThat(responseBody.getData().get(26).getNome()).isEqualTo(StateDomain.TOCANTINS.getName());
        assertThat(responseBody.getData().get(26).getSigla()).isEqualTo(StateDomain.TOCANTINS.getUf());
        assertThat(responseBody.getData().get(26).getIdIBGE()).isEqualTo(StateDomain.TOCANTINS.getIBGEid());

    }

    @Test
    public void shouldReturnAddressForTheCEP200OK() {
        mockViaCEPResponse();
        ResponseEntity<DataHttpResponse<AddressHttpResponse>> response =
                restTemplate.exchange("/enderecos?cep=89281-522",
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<DataHttpResponse<AddressHttpResponse>>(){});

        DataHttpResponse<AddressHttpResponse> responseBody = response.getBody();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        assertThat(responseBody).isNotNull();
        assertThat(responseBody.getData()).isNotNull();
        assertThat(responseBody.getData().getCep()).isEqualTo("89281-522");
        assertThat(responseBody.getData().getLogradouro()).isEqualTo("Avenida dos Imigrantes");
        assertThat(responseBody.getData().getComplemento()).isEqualTo("de 2101 ao fim - lado ímpar");
        assertThat(responseBody.getData().getBairro()).isEqualTo("Progresso");
        assertThat(responseBody.getData().getCidade()).isEqualTo("São Bento do Sul");
        assertThat(responseBody.getData().getUf()).isEqualTo("SC");
    }

    @Test
    public void shouldReturnNotFoundWhenThereIsNoAddressForTheCEP404NotFound() {
        mockViaCEPNotFoundResponse();
        ResponseEntity<DataHttpResponse<AddressHttpResponse>> response =
                restTemplate.exchange("/enderecos?cep=01516-100",
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<DataHttpResponse<AddressHttpResponse>>(){});

        DataHttpResponse<AddressHttpResponse> responseBody = response.getBody();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

        assertThat(responseBody).isNotNull();
        assertThat(responseBody.getMessage()).isEqualTo("Não foi possível obter endereço para o CEP: 01516-100");

    }

    private void mockViaCEPNotFoundResponse() {
        when(viaCepClient.getAddressByCEP("01515-100")).
                thenReturn(Optional.empty());
    }

    @Test
    public void shouldReturnNotFoundWhenThereIsNoCitiesForTheUF404NotFound() {
        mockIBGENotFoundResponse();

        ResponseEntity<DataHttpResponse<AddressHttpResponse>> response =
                restTemplate.exchange("/estados/" + StateDomain.SAO_PAULO.getUf() + "/municipios",
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<DataHttpResponse<AddressHttpResponse>>(){});

        DataHttpResponse<AddressHttpResponse> responseBody = response.getBody();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);

        assertThat(responseBody).isNotNull();
        assertThat(responseBody.getMessage()).isEqualTo("Não foi possível obter municípios para o UF: SP");

    }

    @Test
    public void shouldReturnListOfCitiesForTheUF200OK() {
        mockIBGECitiesOKResponse();
        ResponseEntity<DataHttpResponse<List<ServicosDadosIBGECityHttpResponse>>> response =
                restTemplate.exchange("/estados/SP/municipios",
                        HttpMethod.GET,
                        null,
                        new ParameterizedTypeReference<DataHttpResponse<List<ServicosDadosIBGECityHttpResponse>>>(){});

        DataHttpResponse<List<ServicosDadosIBGECityHttpResponse>> responseBody = response.getBody();
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);

        assertThat(responseBody).isNotNull();
        assertThat(responseBody.getData()).isNotNull();
        assertThat(responseBody.getData().size()).isEqualTo(2);
        assertThat(responseBody.getData().get(0).getNome()).isEqualTo("Foo Bar");
        assertThat(responseBody.getData().get(1).getNome()).isEqualTo("Foo Bar 2");

    }

    private void mockViaCEPResponse() {
        ViaCepAddressHttpResponse responseMocked = new ViaCepAddressHttpResponse();
        responseMocked.setCep("89281-522");
        responseMocked.setLogradouro("Avenida dos Imigrantes");
        responseMocked.setComplemento("de 2101 ao fim - lado ímpar");
        responseMocked.setBairro("Progresso");
        responseMocked.setLocalidade("São Bento do Sul");
        responseMocked.setUf("SC");
        responseMocked.setIbge("4215802");
        responseMocked.setGia("");
        responseMocked.setDdd("47");
        responseMocked.setSiafi("8311");
        ResponseEntity<ViaCepAddressHttpResponse> responseEntity = ResponseEntity.ok(responseMocked);

        when(viaCepClient.getAddressByCEP("89281-522")).
                thenReturn(Optional.of(responseEntity));
    }

    private void mockIBGENotFoundResponse() {
        when(servicosDadosIBGEClient.getCitiesByStateId(StateDomain.SAO_PAULO.getIBGEid())).
                thenReturn(Optional.empty());
    }

    private void mockIBGECitiesOKResponse() {
        List<ServicosDadosIBGECityHttpResponse> cities = mockServicosDadosIBGECityHttpResponses();
        ResponseEntity<List<ServicosDadosIBGECityHttpResponse>> responseEntity = ResponseEntity.ok(cities);
        when(servicosDadosIBGEClient.getCitiesByStateId(StateDomain.SAO_PAULO.getIBGEid())).
                thenReturn(Optional.of(responseEntity));
    }

    private List<ServicosDadosIBGECityHttpResponse> mockServicosDadosIBGECityHttpResponses() {
        ServicosDadosIBGECityHttpResponse servicosDadosIBGECityHttpResponse =
                new ServicosDadosIBGECityHttpResponse();
        servicosDadosIBGECityHttpResponse.setNome("Foo Bar");
        ServicosDadosIBGECityHttpResponse secondServicosDadosIBGECityHttpResponse =
                new ServicosDadosIBGECityHttpResponse();
        secondServicosDadosIBGECityHttpResponse.setNome("Foo Bar 2");
        return Arrays.asList(servicosDadosIBGECityHttpResponse, secondServicosDadosIBGECityHttpResponse);
    }


}