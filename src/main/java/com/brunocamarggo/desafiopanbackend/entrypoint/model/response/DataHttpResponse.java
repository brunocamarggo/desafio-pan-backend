package com.brunocamarggo.desafiopanbackend.entrypoint.model.response;

import com.fasterxml.jackson.annotation.JsonInclude;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;

@JsonInclude(NON_NULL)
public class DataHttpResponse<T> {

    private T data;

    private String message;


    public DataHttpResponse() {
    }

    public DataHttpResponse(T data) {
        this.data = data;
    }

    public T getData() {
        return data;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
