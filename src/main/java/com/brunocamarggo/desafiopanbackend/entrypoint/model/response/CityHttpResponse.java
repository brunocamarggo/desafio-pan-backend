package com.brunocamarggo.desafiopanbackend.entrypoint.model.response;

public class CityHttpResponse {

    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
