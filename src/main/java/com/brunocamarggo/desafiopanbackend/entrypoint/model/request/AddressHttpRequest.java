package com.brunocamarggo.desafiopanbackend.entrypoint.model.request;

import javax.validation.constraints.NotBlank;

public class AddressHttpRequest {

    @NotBlank(message = "O campo 'cep' não pode ser vazio.")
    private String cep;
    @NotBlank(message = "O campo 'logradouro' não pode ser vazio.")
    private String logradouro;
    private String numero;
    private String complemento;
    private String bairo;
    @NotBlank(message = "O campo 'cidade' não pode ser vazio.")
    private String cidade;
    @NotBlank(message = "O campo 'estado' não pode ser vazio.")
    private String estado;

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

    public String getLogradouro() {
        return logradouro;
    }

    public void setLogradouro(String logradouro) {
        this.logradouro = logradouro;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getBairo() {
        return bairo;
    }

    public void setBairo(String bairo) {
        this.bairo = bairo;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
}
