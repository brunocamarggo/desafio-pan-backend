package com.brunocamarggo.desafiopanbackend.entrypoint.model.response;

import com.fasterxml.jackson.databind.PropertyNamingStrategy.SnakeCaseStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(SnakeCaseStrategy.class)
public class StateHttpResponse {

    private Integer idIBGE;
    private String sigla;
    private String nome;

    public Integer getIdIBGE() {
        return idIBGE;
    }

    public void setIdIBGE(Integer idIBGE) {
        this.idIBGE = idIBGE;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
