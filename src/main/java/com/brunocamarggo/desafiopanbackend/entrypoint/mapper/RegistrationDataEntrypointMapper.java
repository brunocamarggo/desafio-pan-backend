package com.brunocamarggo.desafiopanbackend.entrypoint.mapper;

import com.brunocamarggo.desafiopanbackend.entrypoint.model.response.CityHttpResponse;
import com.brunocamarggo.desafiopanbackend.entrypoint.model.response.StateHttpResponse;
import com.brunocamarggo.desafiopanbackend.usecase.domain.CityDomain;
import com.brunocamarggo.desafiopanbackend.usecase.domain.StateDomain;

import java.util.List;
import java.util.stream.Collectors;

public class RegistrationDataEntrypointMapper {

    private RegistrationDataEntrypointMapper() {}

    public static List<StateHttpResponse> toHttpResponse(List<StateDomain> states) {
        return states != null ? states.stream()
            .map(RegistrationDataEntrypointMapper::toHttpResponse)
            .collect(Collectors.toList()) : null;

    }

    public static StateHttpResponse toHttpResponse(StateDomain state) {
        StateHttpResponse stateHttpResponse = new StateHttpResponse();
        stateHttpResponse.setIdIBGE(state.getIBGEid());
        stateHttpResponse.setSigla(state.getUf());
        stateHttpResponse.setNome(state.getName());
        return stateHttpResponse;
    }

    public static List<CityHttpResponse> toCitiesHttpResponse(List<CityDomain> cities) {
        return cities != null ? cities.stream()
                .map(RegistrationDataEntrypointMapper::toHttpResponse)
                .collect(Collectors.toList()) : null;

    }
    public static CityHttpResponse toHttpResponse(CityDomain cityDomain) {
        CityHttpResponse cityHttpResponse = new CityHttpResponse();
        cityHttpResponse.setNome(cityDomain.getName());
        return cityHttpResponse;
    }
}
