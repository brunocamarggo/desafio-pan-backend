package com.brunocamarggo.desafiopanbackend.entrypoint.mapper;

import com.brunocamarggo.desafiopanbackend.entrypoint.model.request.AddressHttpRequest;
import com.brunocamarggo.desafiopanbackend.entrypoint.model.response.AddressHttpResponse;
import com.brunocamarggo.desafiopanbackend.usecase.domain.AddressDomain;
import com.brunocamarggo.desafiopanbackend.usecase.domain.StateDomain;
import com.brunocamarggo.desafiopanbackend.usecase.domain.request.AddressRequestDomain;

public class AddressEntrypointMapper {

    public static AddressHttpResponse toHttpResponse(AddressDomain addressDomain) {
        AddressHttpResponse addressHttpResponse = new AddressHttpResponse();
        addressHttpResponse.setCep(addressDomain.getZipcode());
        addressHttpResponse.setLogradouro(addressDomain.getStreet());
        addressHttpResponse.setNumero(addressDomain.getNumber());
        addressHttpResponse.setComplemento(addressDomain.getAddress2());
        addressHttpResponse.setBairro(addressDomain.getNeighborhood());
        addressHttpResponse.setCidade(addressDomain.getCity());
        addressHttpResponse.setUf(addressDomain.getState().getUf());
        return addressHttpResponse;
    }

    public static AddressRequestDomain toAddressRequestDomain(AddressHttpRequest addressHttpRequest) {
        AddressRequestDomain addressRequestDomain = new AddressRequestDomain();
        addressRequestDomain.setZipcode(addressHttpRequest.getCep());
        addressRequestDomain.setStreet(addressHttpRequest.getLogradouro());
        addressRequestDomain.setNumber(addressHttpRequest.getNumero());
        addressRequestDomain.setAddress2(addressHttpRequest.getComplemento());
        addressRequestDomain.setNeighborhood(addressHttpRequest.getBairo());
        addressRequestDomain.setCity(addressHttpRequest.getCidade());
        addressRequestDomain.setState(StateDomain.findByUF(addressHttpRequest.getEstado()));
        return addressRequestDomain;
    }
}
