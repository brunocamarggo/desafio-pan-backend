package com.brunocamarggo.desafiopanbackend.entrypoint.controller;

import com.brunocamarggo.desafiopanbackend.entrypoint.mapper.AddressEntrypointMapper;
import com.brunocamarggo.desafiopanbackend.entrypoint.mapper.RegistrationDataEntrypointMapper;
import com.brunocamarggo.desafiopanbackend.entrypoint.model.response.AddressHttpResponse;
import com.brunocamarggo.desafiopanbackend.entrypoint.model.response.CityHttpResponse;
import com.brunocamarggo.desafiopanbackend.entrypoint.model.response.DataHttpResponse;
import com.brunocamarggo.desafiopanbackend.entrypoint.model.response.StateHttpResponse;
import com.brunocamarggo.desafiopanbackend.usecase.RegistrationDataUseCase;
import com.brunocamarggo.desafiopanbackend.usecase.domain.CityDomain;
import com.brunocamarggo.desafiopanbackend.usecase.domain.StateDomain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RegistrationDataController {

    private final RegistrationDataUseCase registrationDataUseCase;

    @Autowired
    public RegistrationDataController(RegistrationDataUseCase registrationDataUseCase) {
        this.registrationDataUseCase = registrationDataUseCase;
    }

    @GetMapping("/estados")
    public ResponseEntity<DataHttpResponse<List<StateHttpResponse>>> getStates() {
        return registrationDataUseCase.getStates()
                .map(this::getStatesResponseEntityOK)
                .orElseGet(() -> getResponseEntityNotFound("Não foi possível obter a lista de Estados."));
    }

    @GetMapping("/enderecos")
    public ResponseEntity<DataHttpResponse<AddressHttpResponse>> getAddressByCEP(@RequestParam String cep) {
        return registrationDataUseCase.getAddressByCEP(cep)
                .map(this::getAddressResponseEntityOK)
                .orElseGet(() -> getResponseEntityNotFound("Não foi possível obter endereço para o CEP: " + cep));
    }

    @GetMapping("/estados/{uf}/municipios")
    public ResponseEntity<DataHttpResponse<List<CityHttpResponse>>> getCitiesByState(@PathVariable String uf) {
        return registrationDataUseCase.getCities(StateDomain.findByUF(uf))
                .map(this::getCitiesResponseEntityOK)
                .orElseGet(() -> getResponseEntityNotFound("Não foi possível obter municípios para o UF: " + uf));
    }

    private ResponseEntity<DataHttpResponse<List<StateHttpResponse>>> getStatesResponseEntityOK(List<StateDomain> states) {
        return ResponseEntity.ok(new DataHttpResponse<>(RegistrationDataEntrypointMapper.toHttpResponse(states)));
    }

    private ResponseEntity<DataHttpResponse<AddressHttpResponse>> getAddressResponseEntityOK(com.brunocamarggo.desafiopanbackend.usecase.domain.AddressDomain addressDomain) {
        return ResponseEntity.ok(new DataHttpResponse<>(AddressEntrypointMapper.toHttpResponse(addressDomain)));
    }

    private <T> ResponseEntity<DataHttpResponse<T>> getResponseEntityNotFound(String s) {
        DataHttpResponse<T> httpResponse = new DataHttpResponse<>();
        httpResponse.setMessage(s);
        return new ResponseEntity<>(httpResponse, HttpStatus.NOT_FOUND);
    }

    private ResponseEntity<DataHttpResponse<List<CityHttpResponse>>> getCitiesResponseEntityOK(List<CityDomain> cities) {
        return ResponseEntity.ok(new DataHttpResponse<>(RegistrationDataEntrypointMapper.toCitiesHttpResponse(cities)));
    }
}
