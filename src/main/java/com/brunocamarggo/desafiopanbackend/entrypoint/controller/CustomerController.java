package com.brunocamarggo.desafiopanbackend.entrypoint.controller;

import com.brunocamarggo.desafiopanbackend.entrypoint.mapper.AddressEntrypointMapper;
import com.brunocamarggo.desafiopanbackend.entrypoint.model.request.AddressHttpRequest;
import com.brunocamarggo.desafiopanbackend.entrypoint.model.response.AddressHttpResponse;
import com.brunocamarggo.desafiopanbackend.entrypoint.model.response.DataHttpResponse;
import com.brunocamarggo.desafiopanbackend.usecase.CustomerUseCase;
import com.brunocamarggo.desafiopanbackend.usecase.domain.CustomerDomain;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static com.brunocamarggo.desafiopanbackend.entrypoint.mapper.AddressEntrypointMapper.toAddressRequestDomain;

@RestController
@RequestMapping("/clientes")
public class CustomerController {

    private final CustomerUseCase customerUseCase;

    @Autowired
    public CustomerController(CustomerUseCase customerUseCase) {
        this.customerUseCase = customerUseCase;
    }

    @GetMapping("/{cpf}/endereco")
    public ResponseEntity<DataHttpResponse<AddressHttpResponse>> getCustomerAddress(@PathVariable Long cpf) {
        return customerUseCase.getCustomer(cpf)
                .map(this::getResponseEntityOK)
                .orElseGet(this::getResponseEntityNotFound);
    }

    @PutMapping("/{cpf}/endereco")
    public ResponseEntity<DataHttpResponse<AddressHttpResponse>> updateAddress(@Valid @RequestBody AddressHttpRequest addressHttpRequest, @PathVariable Long cpf) {
        return customerUseCase.updateAddress(toAddressRequestDomain(addressHttpRequest), cpf)
                .map(this::getResponseEntityOK)
                .orElseGet(this::getResponseEntityNotFound);
    }

    private ResponseEntity<DataHttpResponse<AddressHttpResponse>> getResponseEntityOK(CustomerDomain customerDomain) {
        return ResponseEntity.ok(new DataHttpResponse<>(AddressEntrypointMapper.toHttpResponse(customerDomain.getAddress())));
    }

    private ResponseEntity<DataHttpResponse<AddressHttpResponse>> getResponseEntityNotFound() {
        DataHttpResponse<AddressHttpResponse> httpResponse = new DataHttpResponse<>();
        httpResponse.setMessage("Não há endereço para o cliente informado.");
        return new ResponseEntity<>(httpResponse, HttpStatus.NOT_FOUND);
    }
}
