package com.brunocamarggo.desafiopanbackend;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@EnableCaching
@SpringBootApplication
public class DesafioPanBackendApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(DesafioPanBackendApplication.class);

	public static void main(String[] args) {
		LOGGER.info("Starting the application...");
		SpringApplication.run(DesafioPanBackendApplication.class, args);
	}

}
