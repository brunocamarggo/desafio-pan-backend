package com.brunocamarggo.desafiopanbackend.usecase;

import com.brunocamarggo.desafiopanbackend.usecase.domain.CustomerDomain;
import com.brunocamarggo.desafiopanbackend.usecase.domain.request.AddressRequestDomain;
import com.brunocamarggo.desafiopanbackend.usecase.gateway.CustomerGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class CustomerUseCase {

    private final CustomerGateway customerGateway;

    @Autowired
    public CustomerUseCase(CustomerGateway customerGateway) {
        this.customerGateway = customerGateway;
    }

    public Optional<CustomerDomain> getCustomer(Long cpf) {
        return customerGateway.getCustomer(cpf);
    }

    public Optional<CustomerDomain> updateAddress(AddressRequestDomain addressRequestDomain, Long cpf) {
        return customerGateway.updateAddress(addressRequestDomain, cpf);

    }
}
