package com.brunocamarggo.desafiopanbackend.usecase.gateway;

import com.brunocamarggo.desafiopanbackend.usecase.domain.CustomerDomain;
import com.brunocamarggo.desafiopanbackend.usecase.domain.request.AddressRequestDomain;

import java.util.Optional;

public interface CustomerGateway {

    Optional<CustomerDomain> getCustomer(Long cpf);
    Optional<CustomerDomain> updateAddress(AddressRequestDomain addressRequestDomain, Long cpf);
}
