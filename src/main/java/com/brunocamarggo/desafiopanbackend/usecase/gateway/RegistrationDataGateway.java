package com.brunocamarggo.desafiopanbackend.usecase.gateway;

import com.brunocamarggo.desafiopanbackend.usecase.domain.AddressDomain;
import com.brunocamarggo.desafiopanbackend.usecase.domain.CityDomain;
import com.brunocamarggo.desafiopanbackend.usecase.domain.StateDomain;

import java.util.List;
import java.util.Optional;

public interface RegistrationDataGateway {

    Optional<List<CityDomain>> getCitiesByState(StateDomain state);

    Optional<AddressDomain> getAddressByCEP(String cep);
}
