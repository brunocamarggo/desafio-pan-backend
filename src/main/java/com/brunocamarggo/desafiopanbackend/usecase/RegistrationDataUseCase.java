package com.brunocamarggo.desafiopanbackend.usecase;

import com.brunocamarggo.desafiopanbackend.usecase.domain.AddressDomain;
import com.brunocamarggo.desafiopanbackend.usecase.domain.CityDomain;
import com.brunocamarggo.desafiopanbackend.usecase.domain.StateDomain;
import com.brunocamarggo.desafiopanbackend.usecase.gateway.RegistrationDataGateway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class RegistrationDataUseCase {

    private final RegistrationDataGateway registrationDataGateway;

    @Autowired
    public RegistrationDataUseCase(RegistrationDataGateway registrationDataGateway) {
        this.registrationDataGateway = registrationDataGateway;
    }

    public Optional<List<StateDomain>> getStates() {
        return Optional.of(StateDomain.getStatesSorted());
    }

    public Optional<List<CityDomain>> getCities(StateDomain state) {
        return registrationDataGateway.getCitiesByState(state);
    }
    public Optional<AddressDomain> getAddressByCEP(String cep) {
        return registrationDataGateway.getAddressByCEP(cep);
    }
}
