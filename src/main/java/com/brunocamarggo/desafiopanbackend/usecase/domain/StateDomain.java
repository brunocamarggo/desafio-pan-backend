package com.brunocamarggo.desafiopanbackend.usecase.domain;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.stream.Collectors;

public enum StateDomain {

    RONDONIA("Rondônia", 11, "RO", 2),
    ACRE("Acre", 12, "AC", 2),
    AMAZONAS("Amazonas", 13, "AM", 2),
    RORAIMA("Roraima", 14, "RR", 2),
    PARA("Pará", 15, "PA", 2),
    AMAPA("Amapá", 16, "AP", 2),
    TOCANTINS("Tocantins", 17, "TO", 2),
    MARANHAO("Maranhão", 21, "MA", 2),
    PIAUI("Piauí", 22, "PI", 2),
    CEARA("Ceará", 23, "CE", 2),
    RIO_GRANDE_DO_NORTE("Rio Grande do Norte", 24, "RN", 2),
    PARAIBA("Paraíba", 25, "PB", 2),
    PERNAMBUCO("Pernambuco", 26, "PE", 2),
    ALAGOAS("Alagoas", 27, "AL", 2),
    SERGIPE("Sergipe", 28, "SE", 2),
    BAHIA("Bahia", 29, "BA", 2),
    MINAS_GERAIS("Minas Gerais", 31, "MG", 2),
    ESPIRITO_SANTO("Espírito Santo", 32, "ES", 2),
    RIO_DE_JANEIRO("Rio de Janeiro", 33, "RJ", 1),
    SAO_PAULO("São Paulo", 35, "SP", 0),
    PARANA("Paraná", 41, "PR", 2),
    SANTA_CATARINA("Santa Catarina", 42, "SC", 2),
    RIO_GRANDE_DO_SUL("Rio Grande do Sul", 43, "RS", 2),
    MATO_GROSSO_DO_SUL("Mato Grosso do Sul", 50, "MS", 2),
    MATO_GROSSO("Mato Grosso", 51, "MT", 2),
    GOIAS("Goiás", 52, "GO", 2),
    DISTRITO_FEDERAL("Distrito Federal", 53, "DF", 2);

    private final String name;
    private final int IBGEid;
    private final String uf;
    private final int priority;

    StateDomain(String name, int IBGEid, String uf, int priority) {
        this.name = name;
        this.IBGEid = IBGEid;
        this.uf = uf;
        this.priority = priority;
    }

    public String getUf() {
        return uf;
    }

    public int getIBGEid() {
        return IBGEid;
    }

    public String getName() {
        return name;
    }

    public static StateDomain findByUF(String uf) {
        return Arrays.stream(values())
                .filter(stateDomain -> stateDomain.getUf().equals(uf))
                .findFirst()
                .orElseThrow(() -> new NoSuchElementException("Não existe Estado para a Unidade Federativa Informada: " + uf));
    }

    public static List<StateDomain> getStatesSorted() {
        return Arrays.stream(values())
                .sorted(getComparatorByPrioityThenName())
                .collect(Collectors.toList());
    }

    private static Comparator<StateDomain> getComparatorByPrioityThenName() {
        Comparator<StateDomain> comparator = Comparator.comparing(StateDomain::getPriority);
        comparator = comparator.thenComparing(StateDomain::getName);
        return comparator;
    }

    public int getPriority() {
        return priority;
    }

}
