package com.brunocamarggo.desafiopanbackend.usecase.domain;

import java.time.LocalDate;

public class CustomerDomain {

    private String name;
    private LocalDate birthDate;
    private AddressDomain address;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public AddressDomain getAddress() {
        return address;
    }

    public void setAddress(AddressDomain address) {
        this.address = address;
    }
}
