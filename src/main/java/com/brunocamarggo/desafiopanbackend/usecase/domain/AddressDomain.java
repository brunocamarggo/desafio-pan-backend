package com.brunocamarggo.desafiopanbackend.usecase.domain;

public class AddressDomain {

    private String zipcode;
    private String street;
    private String number;
    private String address2;
    private String neighborhood;
    private String city;
    private StateDomain state;

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAddress2() {
        return address2;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public StateDomain getState() {
        return state;
    }

    public void setState(StateDomain state) {
        this.state = state;
    }
}
