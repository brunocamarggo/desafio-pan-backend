package com.brunocamarggo.desafiopanbackend.usecase.domain;

public class CityDomain {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
