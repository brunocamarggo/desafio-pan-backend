package com.brunocamarggo.desafiopanbackend.dataprovider;

import com.brunocamarggo.desafiopanbackend.dataprovider.http.ServicosDadosIBGEClient;
import com.brunocamarggo.desafiopanbackend.dataprovider.http.ViaCepClient;
import com.brunocamarggo.desafiopanbackend.dataprovider.mapper.RegistrationDataDataProviderMapper;
import com.brunocamarggo.desafiopanbackend.usecase.domain.AddressDomain;
import com.brunocamarggo.desafiopanbackend.usecase.domain.CityDomain;
import com.brunocamarggo.desafiopanbackend.usecase.domain.StateDomain;
import com.brunocamarggo.desafiopanbackend.usecase.gateway.RegistrationDataGateway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RegistrationDataDataProvider implements RegistrationDataGateway {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationDataDataProvider.class);

    private final ViaCepClient viaCepClient;
    private final ServicosDadosIBGEClient servicosDadosIBGEClient;

    @Autowired
    public RegistrationDataDataProvider(ViaCepClient viaCepClient, ServicosDadosIBGEClient servicosDadosIBGEClient) {
        this.viaCepClient = viaCepClient;
        this.servicosDadosIBGEClient = servicosDadosIBGEClient;
    }

    @Override
    public Optional<List<CityDomain>> getCitiesByState(StateDomain state) {
        LOGGER.info("trying to retrieve cities by State: {}...", state);
        return servicosDadosIBGEClient.getCitiesByStateId(state.getIBGEid())
                .map(ResponseEntity::getBody)
                .map(RegistrationDataDataProviderMapper::toDomain);
    }

    @Override
    public Optional<AddressDomain> getAddressByCEP(String cep) {
        LOGGER.info("trying to retrieve address by CEP: {}...", cep);
        return viaCepClient.getAddressByCEP(cep)
                .map(ResponseEntity::getBody)
                .map(RegistrationDataDataProviderMapper::toDomain);
    }
}
