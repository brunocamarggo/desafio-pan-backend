package com.brunocamarggo.desafiopanbackend.dataprovider;

import com.brunocamarggo.desafiopanbackend.dataprovider.mapper.CustomerDataProviderMapper;
import com.brunocamarggo.desafiopanbackend.dataprovider.repository.CustomerRepository;
import com.brunocamarggo.desafiopanbackend.dataprovider.repository.entity.CustomerEntity;
import com.brunocamarggo.desafiopanbackend.usecase.domain.CustomerDomain;
import com.brunocamarggo.desafiopanbackend.usecase.domain.request.AddressRequestDomain;
import com.brunocamarggo.desafiopanbackend.usecase.gateway.CustomerGateway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CustomerDataProvider implements CustomerGateway {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerDataProvider.class);

    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerDataProvider(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public Optional<CustomerDomain> getCustomer(Long cpf) {
        LOGGER.info("trying to retrieve customer by CPF: {}...", cpf);
        return customerRepository.findByCpf(cpf)
                .map(CustomerDataProviderMapper::toDomain);
    }

    @Override
    public Optional<CustomerDomain> updateAddress(AddressRequestDomain addressRequestDomain, Long cpf) {
        LOGGER.info("trying to update address of customer {}...", cpf);
        return customerRepository.findByCpf(cpf)
                .map(customerEntity -> updateCustomerAddress(addressRequestDomain, customerEntity));
    }

    private CustomerDomain updateCustomerAddress(AddressRequestDomain addressRequestDomain, CustomerEntity customerEntity) {
        customerEntity.setAddress(CustomerDataProviderMapper.toAddressEntity(customerEntity.getCpf(), addressRequestDomain));
        customerEntity.getAddress().setCustomer(customerEntity);
        CustomerEntity customerUpdated = customerRepository.save(customerEntity);
        return CustomerDataProviderMapper.toDomain(customerUpdated);
    }
}
