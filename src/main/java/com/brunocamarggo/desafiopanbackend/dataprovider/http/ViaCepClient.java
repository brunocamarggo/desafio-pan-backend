package com.brunocamarggo.desafiopanbackend.dataprovider.http;

import com.brunocamarggo.desafiopanbackend.dataprovider.http.model.response.ViaCepAddressHttpResponse;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Optional;

@FeignClient(name = "ViaCepClient", url = "https://viacep.com.br/ws/")
public interface ViaCepClient {

    @Cacheable("address")
    @GetMapping("/{cep}/json")
    Optional<ResponseEntity<ViaCepAddressHttpResponse>> getAddressByCEP(@PathVariable String cep);
}
