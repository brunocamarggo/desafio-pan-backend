package com.brunocamarggo.desafiopanbackend.dataprovider.http;

import com.brunocamarggo.desafiopanbackend.dataprovider.http.model.response.ServicosDadosIBGECityHttpResponse;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;
import java.util.Optional;

@FeignClient(name = "ServicosDadosIBGEClient", url = "https://servicodados.ibge.gov.br/api/v1/localidades/")
public interface ServicosDadosIBGEClient {

    @Cacheable("cities")
    @GetMapping("/estados/{id_estado}/municipios")
    Optional<ResponseEntity<List<ServicosDadosIBGECityHttpResponse>>> getCitiesByStateId(@PathVariable("id_estado") int id);
}
