package com.brunocamarggo.desafiopanbackend.dataprovider.http.model.response;

public class ServicosDadosIBGECityHttpResponse {

    private Integer id;
    private String nome;
    private Microrregiao microrregiao;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Microrregiao getMicrorregiao() {
        return microrregiao;
    }

    public void setMicrorregiao(Microrregiao microrregiao) {
        this.microrregiao = microrregiao;
    }

    public static class Microrregiao {

        private Integer id;
        private String nome;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getNome() {
            return nome;
        }

        public void setNome(String nome) {
            this.nome = nome;
        }

        public static class Mesorregiao {
            private Integer id;
            private String nome;
            private UF UF;

            public Integer getId() {
                return id;
            }

            public void setId(Integer id) {
                this.id = id;
            }

            public String getNome() {
                return nome;
            }

            public void setNome(String nome) {
                this.nome = nome;
            }

            public Mesorregiao.UF getUF() {
                return UF;
            }

            public void setUF(Mesorregiao.UF UF) {
                this.UF = UF;
            }

            public static class UF {
                private Integer id;
                private String sigla;
                private String nome;
                private Regiao regiao;

                public Integer getId() {
                    return id;
                }

                public void setId(Integer id) {
                    this.id = id;
                }

                public String getSigla() {
                    return sigla;
                }

                public void setSigla(String sigla) {
                    this.sigla = sigla;
                }

                public String getNome() {
                    return nome;
                }

                public void setNome(String nome) {
                    this.nome = nome;
                }

                public Regiao getRegiao() {
                    return regiao;
                }

                public void setRegiao(Regiao regiao) {
                    this.regiao = regiao;
                }

                public static class Regiao {
                    private Integer id;
                    private String sigla;
                    private String nome;

                    public Integer getId() {
                        return id;
                    }

                    public void setId(Integer id) {
                        this.id = id;
                    }

                    public String getSigla() {
                        return sigla;
                    }

                    public void setSigla(String sigla) {
                        this.sigla = sigla;
                    }

                    public String getNome() {
                        return nome;
                    }

                    public void setNome(String nome) {
                        this.nome = nome;
                    }
                }
            }
        }


    }
}
