package com.brunocamarggo.desafiopanbackend.dataprovider.repository;

import com.brunocamarggo.desafiopanbackend.dataprovider.repository.entity.CustomerEntity;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CustomerRepository extends JpaRepository<CustomerEntity, Long> {

    @Cacheable("customers")
    Optional<CustomerEntity> findByCpf(Long cpf);
}
