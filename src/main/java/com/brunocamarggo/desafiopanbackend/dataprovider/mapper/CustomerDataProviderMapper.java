package com.brunocamarggo.desafiopanbackend.dataprovider.mapper;

import com.brunocamarggo.desafiopanbackend.dataprovider.repository.entity.AddressEntity;
import com.brunocamarggo.desafiopanbackend.dataprovider.repository.entity.CustomerEntity;
import com.brunocamarggo.desafiopanbackend.usecase.domain.AddressDomain;
import com.brunocamarggo.desafiopanbackend.usecase.domain.CustomerDomain;
import com.brunocamarggo.desafiopanbackend.usecase.domain.StateDomain;
import com.brunocamarggo.desafiopanbackend.usecase.domain.request.AddressRequestDomain;

public class CustomerDataProviderMapper {

    private CustomerDataProviderMapper() { }

    public static CustomerDomain toDomain(CustomerEntity customerEntity) {
        CustomerDomain customerDomain = new CustomerDomain();
        customerDomain.setName(customerEntity.getName());
        customerDomain.setBirthDate(customerEntity.getBirthDate());
        customerDomain.setAddress(toDomain(customerEntity.getAddress()));

        return customerDomain;
    }

    public static AddressDomain toDomain(AddressEntity addressEntity) {
        AddressDomain addressDomain = new AddressDomain();
        addressDomain.setZipcode(addressEntity.getZipcode());
        addressDomain.setStreet(addressEntity.getStreet());
        addressDomain.setNumber(addressEntity.getNumber());
        addressDomain.setAddress2(addressEntity.getAddress2());
        addressDomain.setNeighborhood(addressEntity.getNeighborhood());
        addressDomain.setCity(addressEntity.getCity());
        addressDomain.setState(StateDomain.findByUF(addressEntity.getUf()));
        return addressDomain;
    }

    public static AddressEntity toAddressEntity(Long id, AddressRequestDomain addressRequestDomain) {
        AddressEntity addressEntity = new AddressEntity();
        addressEntity.setId(id);
        addressEntity.setZipcode(addressRequestDomain.getZipcode());
        addressEntity.setStreet(addressRequestDomain.getStreet());
        addressEntity.setNumber(addressRequestDomain.getNumber());
        addressEntity.setAddress2(addressRequestDomain.getAddress2());
        addressEntity.setNeighborhood(addressRequestDomain.getNeighborhood());
        addressEntity.setCity(addressRequestDomain.getCity());
        addressEntity.setUf(addressRequestDomain.getState().getUf());
        return addressEntity;
    }
}
