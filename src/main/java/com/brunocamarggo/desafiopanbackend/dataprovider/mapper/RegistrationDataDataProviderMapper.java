package com.brunocamarggo.desafiopanbackend.dataprovider.mapper;

import com.brunocamarggo.desafiopanbackend.dataprovider.http.model.response.ServicosDadosIBGECityHttpResponse;
import com.brunocamarggo.desafiopanbackend.dataprovider.http.model.response.ViaCepAddressHttpResponse;
import com.brunocamarggo.desafiopanbackend.usecase.domain.AddressDomain;
import com.brunocamarggo.desafiopanbackend.usecase.domain.CityDomain;
import com.brunocamarggo.desafiopanbackend.usecase.domain.StateDomain;

import java.util.List;
import java.util.stream.Collectors;


public class RegistrationDataDataProviderMapper {

    private RegistrationDataDataProviderMapper() {}

    public static AddressDomain toDomain(ViaCepAddressHttpResponse viaCepAddressHttpResponse) {
        AddressDomain addressDomain = new AddressDomain();
        addressDomain.setZipcode(viaCepAddressHttpResponse.getCep());
        addressDomain.setStreet(viaCepAddressHttpResponse.getLogradouro());
        addressDomain.setAddress2(viaCepAddressHttpResponse.getComplemento());
        addressDomain.setNeighborhood(viaCepAddressHttpResponse.getBairro());
        addressDomain.setCity(viaCepAddressHttpResponse.getLocalidade());
        addressDomain.setState(StateDomain.findByUF(viaCepAddressHttpResponse.getUf()));
        return addressDomain;
    }

    public static List<CityDomain> toDomain(List<ServicosDadosIBGECityHttpResponse> servicosDadosIBGECityHttpResponses) {
       return servicosDadosIBGECityHttpResponses.stream()
               .map(RegistrationDataDataProviderMapper::toDomain)
               .collect(Collectors.toList());
    }
    public static CityDomain toDomain(ServicosDadosIBGECityHttpResponse servicosDadosIBGECityHttpResponse) {
        CityDomain cityDomain = new CityDomain();
        cityDomain.setName(servicosDadosIBGECityHttpResponse.getNome());
        return cityDomain;
    }
}
