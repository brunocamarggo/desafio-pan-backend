# Avaliação para time Tecnologia Digital
### Conceitos e Experiências – BackEnd
`Por Bruno Camargo.`

# Introdução
Prezados, inicialmente muito obrigado pelo convite para execução desta avalidação. Fico grato pela concessão do tempo de vocês para avaliar a minha escrita e forma de solucionar problemas do dia-a-dia de desenvolvimento. Fico no aguardo do `feedback`.

# Instalação
### Via Docker
Caso tenha Docker instalado na sua máquina, é possível fazer o startup da aplicação utilizando a imagem pré buildada que deixei disponível em meu DockerHub.

`$ docker run -p 8080:8080 brunocamarggo/desafio-pan-backend:1.0`

Ou caso queria gerar o pacote,

`$ mvn package && java -jar target/desafio-pan-backend-0.0.1-SNAPSHOT.jar`

Para facilitar, disponibilizei junto da aplicação uma collection do Postman. Está na raiz do projeto.
### Estrutura da aplicação
```
└───desafiopanbackend
    ├───dataprovider # Camada responsável por obter e armazenar dados. Geralmente é a implmentação de algum Gateway da camada Use Case.
    ├───entrypoint => Camada que cuida da responsabilidade de fronteiras da aplicação com o "mundo" externo.
    └───usecase => Camada que contém a abstração das regras de negócio
        ├───domain => Entidades que representam artefatos de negócio.
        └───gateway => Contrato que define como deve ser a implementação das regras de negócio.
```
## Explicação da solução adotada para cada cenário.
### Cenário 1 - Consultar Cliente
O título do cenário diz "Consultar Cliente", porém a descrição solicita um `endpoint` capaz de consultar o endereço do cliente. Portanto, foi implementado a rota `clientes/{cpf}/endereço`.

> **Um pequeno adendo**: Em tempos de Lei Geral de Proteção de Dados (LGPD), será que fazer o CPF do cliente uma `key` de consulta é a melhor solução? Se eu tivesse oportunidade de fazer parte do time de idelização da `API`, aconselharia o uso de um identificador universal (UUID) para identificar os clientes, evitando assim a exposição de dados sensíveis do cliente. Mas como eu entendo que esta avaliação possui carácter "didáticos", optei por construir o que foi solicitado e expor aqui a minha concepção sobre o tema.

> Outro ponto fundamental é a representação de domínio da API. Aqui foi construído uma API que cuidade de vários domínios: clientes, endereços, estads e municípios. Pensando um parque robusto de microsserviços, a melhor solução seria criar várias aplicações isolando por domínio afim de facilitar o desenvolvimento, manutenção, escabilidade, etc. Mas uma vez, não é impraticável a solução da avaliação criando várias aplicações, portanto foi abordado um contexto simplista na solução. Porém acho válido citar a preocupação com Domain-driven Design.

### Cenário 2 - Consultar CEP
Para a solução deste cenário apenas foi criado uma cama anticorrupção do serviço disponível pelo próprio ViaCEP (`https://viacep.com.br/ws/{cep}/json`), assim nossos canais não precisam conhecer contratos de `APIs` externas, apenas precisam saber da existência do endpoint `/enderecos?cep={cep}`. Como a consulta de `CEP` é um recurso que é passível de `cache`, por questões de simplicidade (e tempo), foi utilizando a implementação de `simple cache` do `Spring` para evitar o disperdicío de tempo em consulta de `CEPs` que já foram realizadas.

### Cenário 3 – Consultar Estados
Olhando para a sugestão disponível sessão "obsevação" da avaliação, é sugerido utilizar a o endpoint `https://servicodados.ibge.gov.br/api/v1/localidades/estados/` para a obtenção de todos os Estados brasileiros. Porém, optei por representar essa necessidade de negócio através do `enum` `StateDomain`. Uma vez que a frequência criação/edição de Estados no Brasil é remota, ao meu ver, não se faz sentindo consumir um recurso externo para a obteção deste recurso toda que vez que nossos canais consumidores necessitam de tal informação. 

Portanto, o endpoint `/estados` contém a representação dessa necidade de negócio. Como é solicitado um mecânismo de ordenação por prioridade, faz sentido a entidade de representação da camada de négocio conter uma solução para isso. Assim, o método `getStatesSorted` de `StateDomain` cuida de elencar os Estados conforme a necessidade de ordenação solicitada.

### Cenário 4 – Consultar Municípios
Solução adotado basicamente idêntica ao do `cénario 2`. Porém, como explicado no tópico acima, a entidade de presentação de negócio `StateDomain` contém o identificador do sistema `IBGE` é que utilizado na camada de anticorrupção para fazer uso do recurso do `IBGE`. Assim, nossos canais precisam apenas conhecer as siglas das unidades federativas, ficando totalmente transparente para eles a complexidade de idetificação presente nos sistemas do `IBGE`.

### Bônus: Cenário 5 – Alterar endereço
Para a execução deste cenário existem pelo menos duas abordagem tratando-se de `REST`. A primeira e mais simples, seria criar uma rota `POST` `clientes` recebendo no corpo da requisição os dados do `cliente`. A segunda, criar uma rota `PUT` `clientes/{cpf}/endereco` recendo apenas os dados referente ao sub recurso de clientes `endereço`. Uma vez que o requisito era apenas a edição de dados refente ao endereço do cliente, optei pela a escolha da segunda forma. A escolha da primeira forma por sem plaúsivel em cenários de retulização de `endpoints`, assim a mesma rota `POST` `/clientes` fica encarreguada de criar ou editar (caso exista) um cliente.

## Requisitos Essenciais:
- Linguagem JAVA: optei por um viés mais funcional, portando é necessário o uso de JDK 1.8+ (Java 8+);
- Framework: Spring Boot: utilizei as facilidades que o framework apresenta como validação de campos, injeção de dependência e controle de fluxo de exceções;
- Mock: Mockito, basicamente no contexto de testes não faz sentido consumir os recursos externos, então com Mockito foi possível simular o retorno dos recursos para prever o comportamento da API desenvolvida;
- RESTFull: O design da `API` segue as diretrizes da arquitetura `REST`.
- Database em memória: O `Database H2` foi utilizado para a construção de uma base em mémoria SQL.

### Documentação

### GET - /clientes/{cpf}/endereco
Retorna os dados refente ao endereço de um cliente buscando por seu CPF.
**Retorno 200 OK**:
```json
{
    "data": {
        "cep": "01516-100",
        "logradouro": "Av. Do Estado",
        "numero": "5814",
        "complemento": "",
        "bairro": "Cambuci",
        "cidade": "São Paulo",
        "uf": "SP"
    }
}
```
Em caso de não existência do recurso é retornado `404 Not Found`.

### GET - /endereços?cep={cep}
Retorna os dados referente a uma localidade buscando pelo atributo obrigaório `CEP`.
**Retorno 200 OK**:
```json
{
    "data": {
        "cep": "01516-100",
        "logradouro": "Avenida do Estado",
        "complemento": "de 5702 a 7000 - lado par",
        "bairro": "Cambuci",
        "cidade": "São Paulo",
        "uf": "SP"
    }
}
```
Em caso de não existência do recurso é retornado `404 Not Found`. Em caso de tentativa de consumo da rota sem envio do parâmetro `CEP`, o `HTTP status code` `400 Bad Request` é retornado.

### GET - /estados
Retorna a lista de Estados presente no Brasil. Para a ordenação é considerado São Paulo e Rio de Janeiro como os primeiros da lista, respectivamente, seguindo de ordem alfabética.
**Retorno 200 OK**:
```json
{
    "data": [
        {
            "id_ibge": 35,
            "sigla": "SP",
            "nome": "São Paulo"
        },
        {
            "id_ibge": 33,
            "sigla": "RJ",
            "nome": "Rio de Janeiro"
        },
        {
            "id_ibge": 12,
            "sigla": "AC",
            "nome": "Acre"
        }
        ...
    ]
}
```
### GET - /estados/{sigla}/municipios
Retorna a lista de municípos presentes em um Estado brasileiro. O parâmetro `sigla` é a sigla da unidade federativa, como por exempo `SP`.
**Retorno 200 OK**:
```json
{
    "data": [
        {
            "nome": "Adamantina"
        },
        {
            "nome": "Adolfo"
        },
        {
            "nome": "Aguaí"
        },
        ...
    ]
}
```
Caso não existão municípios para a `sigla` informada, então é retornado `404 Not Found`.

### PUT - /clientes/{cpf}/endereco
Atualiza o endereço de um determinado cliente fazendo uso do seu `CPF` para a atualização. O corpo da requisição deve respeitar necessáriamente a estrutura:
```json
{
    "cep": "19800360",
    "logradouro": "Rua Jose Miranda", 
    "cidade": "Assis",
    "estado": "SP",
    "numero": "1234",
    "complemento": "",
    "bairro": ""
}
```
Sendo os campos: `cep`, `logradouro`, `cidade` e `estado` obrigatórios.
**Retorno 200 OK**:
```json
{
    "data": {
        "cep": "19800360",
        "logradouro": "Cidade Put Teste",
        "numero": "1234",
        "cidade": "Assis",
        "uf": "SP",
    }
}
```
Caso não exista o cliente na base com o `CPF` informado, então é tornado `404 Not Found`.

